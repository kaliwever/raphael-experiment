<?php
    header('Content-Type: image/jpeg');

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $url = urldecode($_GET['url']);
    $width = $_GET['width'];

    //$url = "https://media.nu.nl/m/th0xzb0a8x8k.jpg/schutter-luik-doodde-kennis-uren-hij-politieagenten-doodschoot.jpg";
    //$width = 600;

    // Loading the image and getting the original dimensions
    $image = imagecreatefromjpeg($url);
    $orig_width = imagesx($image);
    $orig_height = imagesy($image);

    // Calc the new height
    $height = (($orig_height * $width) / $orig_width);

    // Create new image to display
    $new_image = imagecreatetruecolor($width, $height);

    // Create new image with changed dimensions
    imagecopyresized($new_image, $image,
        0, 0, 0, 0,
        $width, $height,
        $orig_width, $orig_height);

    // Print image
    imagejpeg($new_image);
?>