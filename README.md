# Raphael Experimenten

### Checklist
- Sandbox maken icm Woocommerce
	- Data meesturen in winkelwagen
	- SVG Exporteren
	
- Basis functionaliteiten
    - Custom_meta bij product als het een door gebruiker aangepast ontwerp is
	- Custom_meta houdt JSON bij
	- Producteditor slaat Paper als JSON op in custom_meta bij product
	- Winkelmand, checkout toont SVG over afbeelding doormiddel van JSON export naar SVG en import vanuit JSON vanuit custom_meta
	- Mail toon eventueel een gegenereerde png als thumbnail
	- Mail geeft in ieder geval een link naar een PHP api die op basis van het order id de custom_meta naar boven haalt en hiervan door de JSON een SVG exporteert in de browser

* * *

### Data meesturen in winkelwagen resources:
- https://stackoverflow.com/questions/5433806/convert-embedded-svg-to-png-in-place
- http://bl.ocks.org/curran/7cf9967028259ea032e8
- Als verborgen veld bij het product (soort van notitie maar dan json data, dit dan in de woocommerce mail als een download link meesturen)
- https://stackoverflow.com/questions/1623242/problem-with-dynamic-svg-image-generation

### SVG Exporteren resources:
- http://dmitrybaranovskiy.github.io/raphael/
- https://stackoverflow.com/questions/10120975/how-to-save-an-svg-generated-by-raphael
- https://github.com/AliasIO/Raphael.Export

### Overige resources:
- https://www.pootlepress.com/2014/04/100-woocommerce-tips-tricks/ (order meta box, hier kan ik de svg's misshien in stoppen)