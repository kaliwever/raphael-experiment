<?php

header("Access-Control-Allow-Origin: *");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

$company = Trim(stripslashes($_POST['company']));
$business = Trim(stripslashes($_POST['business']));
$name = Trim(stripslashes($_POST['name']));
$lastname = Trim(stripslashes($_POST['lastname']));
$email = Trim(stripslashes($_POST['email']));
$website = Trim(stripslashes($_POST['website']));
$message = Trim(stripslashes($_POST['message']));

$Username = Trim(stripslashes($_POST['username'])); // Spampreventie


// prepare email body text
$Body = "<html>";

$Body .= "<b>Bedrijfsnaam:</b> ";
$Body .= "<br/>";
$Body .= $company;
$Body .= "<br/>";
$Body .= "<br/>";

$Body .= "<b>Business:</b> ";
$Body .= "<br/>";
$Body .= $business;
$Body .= "<br/>";
$Body .= "<br/>";

$Body .= "<b>Naam:</b> ";
$Body .= "<br/>";
$Body .= $name . " " . $lastname;
$Body .= "<br/>";
$Body .= "<br/>";

$Body .= "<b>E-mail:</b> ";
$Body .= "<br/>";
$Body .= "<a href='mailto:" . $email . "'>";
$Body .= $email;
$Body .= "</a>";
$Body .= "<br/>";
$Body .= "<br/>";

$Body .= "<b>Website:</b> ";
$Body .= "<br/>";
$Body .= $website;
$Body .= "<br/>";
$Body .= "<br/>";

$Body .= "<b>Bericht:</b> ";
$Body .= "<br/>";
$Body .= $message;

$Body .= "</html>";


$textMessage = "Bedrijfsnaam: ";
$textMessage .= $company;
$textMessage .= ". ";

$textMessage = "Business: ";
$textMessage .= $business;
$textMessage .= ". ";

$textMessage = "Naam: ";
$textMessage .= $name . " " . $lastname;
$textMessage .= ". ";

$textMessage = "E-mail: ";
$textMessage .= $email;
$textMessage .= ". ";

$textMessage = "Website: ";
$textMessage .= $website;
$textMessage .= ". ";

$textMessage = "Bericht: ";
$textMessage .= $message;
$textMessage .= ". ";



$mail = new PHPMailer(true);

try{

    if($name == "" || $email == ""){ // Alle of een paar verplichte velden
        
        // Als er direct naar het PHP bestand wordt gelinkt
        header("refresh:7;url=https://www.erkens-themoodmakers.com");
        echo    "Something didn't go right, the form isn't filled in correctly. </br>
                You will be redirected to the homepage in a couple of seconds. </br> </br>
                Or click <a href='https://www.erkens-themoodmakers.com'>here</a> to go right away.";
        exit;

    } else {

        if( $Username == "" ){

            $mail->isSMTP();
            //$mail->SMTPDebug = 1;
            $mail->Host = 'mail.tegendraads-smtp.nl';
            $mail->SMTPAuth = true;
            $mail->Username = 'klanten@tegendraads-smtp.nl';
            $mail->Password = 'TijdelijkWachtwoord2018';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            //$mail->SMTPSecure = 'tsl';
            //$mail->Port = 587;

            $mail->setFrom('klanten@tegendraads-smtp.nl', 'Webformulier [klantnaam]');
            $mail->addAddress('info@[klantdomein]', '[klantnaam]');
            //$mail->addAddress('kali.wever@gmail.com', 'Webformulier test [klantnaam]');
            $mail->addReplyTo($email);

            //Content
            $mail->isHTML(true);
            $mail->Subject = 'Contactformulier website ingevuld';
            $mail->Body = $Body;
            $mail->AltBody = $textMessage;

            $mail->send();

           if(isset($_GET['ajax'])) { // Als het formulier goed is ingevuld weg via javascript

                echo    "<h4>Dear ". $name .",</h4>
                        <p>Thank you so much for your email. </br>
                        Notice that we received your message in perfect order we'll reply to you within 24 hours. </br>
                        </br>
                        Warmest Regards </br>
                        Erkens the mood makers</p>";
                exit;
            } else { // als javascript niet ondersteund wordt, of het ergens niet goed gaat met javascript

                header("refresh:7;url=[klanturl]");
                echo    "Something didn't go right, the form isn't filled in correctly. </br>
                        You will be redirected to the homepage in a couple of seconds. </br> </br>
                        Or click <a href='[klanturl]'>here</a> to go right away.";
                exit;
            }

        }

    }

} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;

    exit;
}


?>