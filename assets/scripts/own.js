$(document).ready(function(){

	/* Global variables
	-----------------------*/
	let screenHeight, screenWidth;
	const $window = $(window);
	const $document = $(document);
	const $html = $('html');


	/* Raphael Init
	------------------------*/
	if( $('#canvas').length > 0 ){

		let paper = Raphael('canvas');
		let paperOptions = { 
			keepRatio: true,
			distance: 1.6,
			draw: 'bbox',
			attrs: { }
		};
		// Afhankelijk van verhouding
		paper.setViewBox(0, 0, 500, 500, true);

		/* Todo
		 * --
		 * //Add Fonts (beter met print ipv text)
		 * Custom shape en png met fatsoenlijk formaat importeren in frame
		 * Filename meesturen als parameter en opslaan als filename
		 * // bbox or circle based on hover
		 * --
		*/

		let allLayers = [];
		let currentLayer = false;
		let currentColor = '#ffffff';
		let editMode;

		let exportedJson;
		if( localStorage.getItem('layer') != null ){
			exportedJson = JSON.parse(localStorage.getItem('layer'));
		}
		console.log(exportedJson);

		const removeCurrentLayer = layer => {
			return function(){
				//console.log(allLayers);
				//console.log(layer.id);
				
				for( var i = 0; i < allLayers.length; i++ ){
					if( allLayers[i].id == layer.id ){
						layer.remove();
						allLayers.splice(i,1);
					}
				}

				if( allLayers.length == 0 ){
					hideEditTools();
					currentLayer = false;
				} else {
					//console.log(allLayers);
					//console.log(allLayers[allLayers.length-1]);
					currentLayer = allLayers[allLayers.length-1];
					//console.log(currentLayer);
					paper.freeTransform(currentLayer, paperOptions);
					//console.dir(currentLayer);
				}
			}
		}
		const showEditTools = () => {
			editMode = true;
			$('li.delete').removeClass('hidden');
			$('li.to-back').removeClass('hidden');
			$('li.to-front').removeClass('hidden');
		}
		const hideEditTools = () => {
			editMode = false;
			$('li.delete').addClass('hidden');
			$('li.to-back').addClass('hidden');
			$('li.to-front').addClass('hidden');
		}

		$('#canvas').on('click', (e) => {
			if( e.target.nodeName == 'svg' && currentLayer ){
				currentLayer.freeTransform.unplug();
				currentLayer = false;

				hideEditTools();
			}
		});

		const addShape = (shapeToAdd) => {

			//if(allLayers.length < 10){

				// Set shap variable
				let shape;

				// Unplug currentLayer if is set
				if( currentLayer ){
					currentLayer.freeTransform.unplug();
				}

				// Determine base shape
				if(shapeToAdd == 'rectangle'){
					shape = paper.rect(200, 200, 100, 100);
				} else if(shapeToAdd == 'circle'){
					shape = paper.circle(250, 250, 75);
				} else if(shapeToAdd == 'hourglass'){
					const hourglassPath = "M27.777,18.941c0.584-0.881,0.896-1.914,0.896-2.998c0-1.457-0.567-2.826-1.598-3.854l-6.91-6.911l-0.003,0.002c-0.985-0.988-2.35-1.6-3.851-1.6c-1.502,0-2.864,0.612-3.85,1.6H12.46l-6.911,6.911c-1.031,1.029-1.598,2.398-1.598,3.854c0,1.457,0.567,2.826,1.598,3.854l6.231,6.229c0.25,0.281,0.512,0.544,0.789,0.785c1.016,0.961,2.338,1.49,3.743,1.49c1.456,0,2.825-0.565,3.854-1.598l6.723-6.725c0.021-0.019,0.034-0.032,0.051-0.051l0.14-0.138c0.26-0.26,0.487-0.54,0.688-0.838c0.004-0.008,0.01-0.015,0.014-0.021L27.777,18.941zM26.658,15.946c0,0.678-0.197,1.326-0.561,1.879c-0.222,0.298-0.447,0.559-0.684,0.784L25.4,18.625c-1.105,1.052-2.354,1.35-3.414,1.35c-0.584,0-1.109-0.09-1.523-0.195c-2.422-0.608-5.056-2.692-6.261-5.732c0.649,0.274,1.362,0.426,2.11,0.426c2.811,0,5.129-2.141,5.415-4.877l3.924,3.925C26.301,14.167,26.658,15.029,26.658,15.946zM16.312,5.6c1.89,0,3.426,1.538,3.426,3.427c0,1.89-1.536,3.427-3.426,3.427c-1.889,0-3.426-1.537-3.426-3.427C12.886,7.138,14.423,5.6,16.312,5.6zM6.974,18.375c-0.649-0.648-1.007-1.512-1.007-2.429c0-0.917,0.357-1.78,1.007-2.428l2.655-2.656c-0.693,2.359-0.991,4.842-0.831,7.221c0.057,0.854,0.175,1.677,0.345,2.46L6.974,18.375zM11.514,11.592c0.583,4.562,4.195,9.066,8.455,10.143c0.693,0.179,1.375,0.265,2.033,0.265c0.01,0,0.02,0,0.027,0l-3.289,3.289c-0.648,0.646-1.512,1.006-2.428,1.006c-0.638,0-1.248-0.177-1.779-0.5l0.001-0.002c-0.209-0.142-0.408-0.295-0.603-0.461c-0.015-0.019-0.031-0.026-0.046-0.043l-0.665-0.664c-1.367-1.567-2.227-3.903-2.412-6.671C10.669,15.856,10.921,13.673,11.514,11.592";
					shape = paper.path(hourglassPath, 200, 200);
				} else if( shapeToAdd == 'text' ){
					const customText = $('#text').val();
					if( customText != '' ){
						/* Of print */
						// shape.print(250, 250, "print", paper.getFont("Open Sans"), 70)

						/* Of text */
						shape = paper.text(250, 250, customText);
						shape.attr({
							'font-size': "70",
							'font-family': 'Warnes'
						});

					} else {
						alert('vul aub eerst wat tekst in');
					}
				}

				//Set base attributes
				shape.attr({
					'fill': currentColor,
					'stroke-width': '0',
					'opacity': '0'
				});

				// Animate base attributes
				shape.animate({
					opacity: 1,
				}, 150, 'cubic-bezier(.8,.5,.25,1)');
				

				//Event Handlers
				let hoveredLayer = false;
				let mouseoutTimer;

				shape.click(function(){
					if( currentLayer ){
						currentLayer.freeTransform.unplug();
					}

					/*
					if( hoveredLayer ){
						console.log(hoveredLayer);
						hoveredLayer.freeTransform.unplug();
					}
					*/

					currentLayer = this;
					//paperOptions.draw = ['bbox'];
					paper.freeTransform(currentLayer, paperOptions);
					showEditTools();

				});
				/** /
				shape.mouseover(function(){
					console.log('mouseover');

					clearTimeout(mouseoutTimer);
					hoveredLayer = this;

					if(this.id != currentLayer.id){
						paperOptions.draw = ['circle'];
						paper.freeTransform(hoveredLayer, paperOptions);
					}

					console.log(currentLayer);
				});
				shape.mouseout(function(){
					console.log('mouseout');

					console.log(this.id + " " + currentLayer.id);
					if(this.id != currentLayer.id){
						console.log('should not get here');
						
						let _this = this;
						const unplug = () =>{
							_this.freeTransform.unplug();
							paperOptions.draw = ['bbox'];
						}
						unplug();
						hoveredLayer = false;
						
					}
					
					console.log(currentLayer);
				});
				/**/


				//Object manager
				paper.freeTransform(shape, paperOptions);
				allLayers.push(shape);
				currentLayer = allLayers[allLayers.length-1];
				showEditTools();
				

			//} else {
				//alert('U heeft teveel lagen aangemaakt (max 10)!');
			//}
		}

		$('.menu li.color').on('click', (e) => {
			const newColor = $(e.currentTarget).find('span').css('backgroundColor');
			currentColor = newColor;
			console.log(newColor);
			for( let i = 0; i < allLayers.length; i++ ){
				//allLayers[i].attr('fill', newColor);
				setTimeout(function(){
					allLayers[i].animate({fill: newColor}, 150, 'cubic-bezier(.8,.5,.25,1)');
				}, i*15);
			}
		});

		$('.menu li.add-rect').on('click', () => {
			addShape('rectangle', paper);
		});

		$('.menu li.add-circ').on('click', () => {
			addShape('circle');
		});

		$('.menu li.add-hourglass').on('click', () => {
			addShape('hourglass');
		});

		$('.menu li.add-text').on('click', () => {
			addShape('text');
		});

		$('.menu li.clear').on('click', () => {
			paper.clear();
			hideEditTools();
		});

		$('.menu li.save').on('click', () => {
			if( currentLayer ){
				currentLayer.freeTransform.unplug();
				currentLayer = false;

				hideEditTools();
			}
			exportedJson = paper.toJSON();
			localStorage.setItem('layer', JSON.stringify(exportedJson));
			let exportedJsonString = JSON.stringify(exportedJson);
			$('.svg-downloader a').attr('href', svgDownloaderLink+'?svg='+exportedJson);
		});

		$('.menu li.load').on('click', () => {
			paper.clear();
			paper.fromJSON(exportedJson);
		});

		/* --- Edit Mode ---*/

		/* Only when selected */
		$('.menu li.delete').on('click', () => {

			const xAx = currentLayer.freeTransform.attrs.translate.x;
			const yAx = currentLayer.freeTransform.attrs.translate.y;
			const scaleFactor = currentLayer.freeTransform.attrs.scale.x * 0.75;
			const rotation = currentLayer.freeTransform.attrs.rotate;

			currentLayer.freeTransform.unplug();
			currentLayer.animate({ opacity:0, transform: 't'+xAx+','+yAx+'s'+scaleFactor+'r'+rotation }, 150, 'ease', removeCurrentLayer(currentLayer));
			
		});

		$('.menu li.to-back').on('click', () => {
			currentLayer.toBack();
		});
		$('.menu li.to-front').on('click', () => {
			currentLayer.toFront();
		});
		

		$document.keydown(function(e){
			if(editMode){

				e.preventDefault();

				if( e.keyCode == 46 || e.keyCode == 8){ // backspace & delete
					
					const xAx = currentLayer.freeTransform.attrs.translate.x;
					const yAx = currentLayer.freeTransform.attrs.translate.y;
					const scaleFactor = currentLayer.freeTransform.attrs.scale.x * 0.75;
					const rotation = currentLayer.freeTransform.attrs.rotate;

					currentLayer.freeTransform.unplug();
					currentLayer.animate({ opacity:0, transform: 't'+xAx+','+yAx+'s'+scaleFactor+'r'+rotation }, 150, 'ease', removeCurrentLayer(currentLayer));


				} else if( e.keyCode ==  37){ // left

					currentLayer.freeTransform.attrs.translate.x -= 10;
					currentLayer.freeTransform.apply();

				} else if( e.keyCode ==  38){ // up

					currentLayer.freeTransform.attrs.translate.y -= 10;
					currentLayer.freeTransform.apply();

				} else if( e.keyCode ==  39){ // right

					currentLayer.freeTransform.attrs.translate.x += 10;
					currentLayer.freeTransform.apply();
					
				} else if( e.keyCode ==  40){ // down

					currentLayer.freeTransform.attrs.translate.y += 10;
					currentLayer.freeTransform.apply();

				}
			}
		});



		const exportedPaper = Raphael('frame');
		let frameWidth = $('#frame').width();
		let frameHeight = $('#frame').height();
		
		$('.exported ul.buttons li.load').on('click', () => {
			exportedPaper.setViewBox(0,0, 500, 500);
			exportedPaper.fromJSON(exportedJson);
		});

		let svgDownloaderLink = $('.svg-downloader a').attr('href');
		let exportedJsonString = JSON.stringify(exportedJson);
		let filename = 'generated-file.svg';
		$('.svg-downloader a').attr('href', svgDownloaderLink+'?svg='+exportedJsonString+'&filename='+filename);

	}









	/* SVG Downloader
	----------------------*/
	if( $('#preview').length > 0 ){
		const url = decodeURIComponent(window.location.href);
		const JSONSVG = url.split('?svg=')[1];
		const previewPaper = Raphael('preview');
		previewPaper.fromJSON(JSON.parse(JSONSVG));

		let previewWidth = $('#preview').width();
		let previewHeight = $('#preview').height();
		previewPaper.setViewBox(0, 0, 500, 500, true);

		// Export dimensions are eight is set in CSS
		const exportPaper = Raphael('export');
		exportPaper.fromJSON(JSONSVG);
		const svgString = exportPaper.toSVG();
		const blob = new Blob([svgString], {"type": "image/svg+xml"});
		const a = document.createElement('a');
			a.download = 'mySvg.svg';
			a.type = 'image/svg+xml';
			a.href = (window.URL || webkitURL).createObjectURL(blob);

		/* Manual download */
		a.innerHTML = 'Download SVG';
		document.getElementById('preview-holder').appendChild(a);

		/* Automatic download */
		//a.click();
	}




	/* Layout
	-----------------------*/
	var layout = function(){
		screenHeight = $window.innerHeight();
		screenWidth = $window.innerWidth();
	}

	/* Initialize functions
	-----------------------*/
	layout();
	$window.resize(layout());

});